import Data.Word (Word8)
import Data.Maybe

type Point = (Int, Int)
type Color = (Word8, Word8, Word8)
-- Painter is something like "draw a circle" or "draw a line"
type Painter = Point -> Maybe Color

data Canvas = Canvas { painters :: [Painter], getWidth :: Int, getHeight :: Int, getBackgroundColor :: Color }


-- Render to
-- 1. ASCII to STDOUT
-- 2. To a file (jpg, png, PPM)
-- 3. To a window

groupN :: Int -> [a] -> [[a]]
groupN n ls = go n ls []
  where
    go n [] acc = reverse acc
    go n ls acc = go n (drop n ls) (take n ls : acc)



renderASCIILines :: Canvas -> [String]
renderASCIILines canvas = map concat $ groupN (getWidth canvas) $ do
  row <- [0..(getHeight canvas - 1)]
  col <- [0..(getWidth canvas - 1)]
  return $ headOrSpace $ mapMaybe (applyPainter row col) (painters canvas)
  where
    applyPainter row col painter = painter (col, row)
    headOrSpace [] = " "
    headOrSpace ((0, 0, 0):_) = " "
    headOrSpace (color:_) = "#" -- "O" "D" "I"

renderASCII :: Canvas -> IO Bool
renderASCII canvas = do
  let lines = renderASCIILines canvas
  putStrLn $ unlines lines
  return True


applyPainters :: Color -> Point -> [Painter] -> Color
applyPainters defaultColor _ [] = defaultColor
applyPainters defaultColor point painters = headOrDefault $ mapMaybe (applyPainter point) painters
  where
    applyPainter pointer painter = painter point
    headOrDefault [] = defaultColor
    headOrDefault (el:_) = el

colorToString :: Color -> String
colorToString (r, g, b) = show r ++ "  " ++ show g ++ " " ++ show b


makePPMContents :: Canvas -> [String]
makePPMContents canvas = ["P3", show (getWidth canvas) ++ " " ++ show (getHeight canvas), "255"] ++ do
  row <- [0..(getHeight canvas - 1)]
  col <- [0..(getWidth canvas  - 1)]
  return $ colorToString $ applyPainters (getBackgroundColor canvas) (col, row) (painters canvas)

-- Create a file, write canvas to file in PPM format, and save
-- Return true if it succeeds, false if it fails
renderPPM :: String -> Canvas -> IO Bool
renderPPM filename canvas = do
  writeFile filename $ unlines $ makePPMContents canvas
  return True


paintCircle :: Color -> Point -> Int -> Painter
paintCircle color center radius = \(x, y) ->
  if dist2 (x, y) center < radius * radius
  then Just color
  else Nothing
  where
    dist2 (x, y) (cx, cy) = dx * dx + dy * dy
      where
        dx = x - cx
        dy = y - cy


-- Test data
width = 1600
height = 1200
white = (200, 125, 225)
center = (div width 2, div height 2)
radius = div width 10
backgroundColor = (0, 0, 150)

circlePainter = paintCircle white center radius
circle2 = paintCircle (255, 0, 0) (div width 3, div height 3) (div width 4)
-- circles = [circle2, circlePainter]
circles = do
  i <- [1..5]
  j <- [1..5]
  return $ paintCircle (50 * i, 125, 255 - 50 * i) ((div width (fromIntegral 10) * fromIntegral i) + div width 3, div height (fromIntegral $ i)) (div width (fromIntegral $ i + j + 10))

testCanvas = Canvas { painters = circles, getWidth = width, getHeight = height, getBackgroundColor = backgroundColor }


asciiCanvas = Canvas { painters = [paintCircle (255, 255, 255) (12, 25) 5,
                                   paintCircle (255, 255, 255) (25, 12) 7],
                       getWidth = 100, getHeight = 50, getBackgroundColor = (255, 255, 255) }

testFunction x
  | x < 5 = Nothing
  | otherwise = Just x



-- Mandelbrot set
complexMultiply :: (Float, Float) -> (Float, Float) -> (Float, Float)
complexMultiply (x1, y1) (x2, y2) = (x1 * x2 - y1 * y2, x1 * y2 + y1 * x2)

(|*) z1 z2 = complexMultiply z1 z2

complexAdd :: (Float, Float) -> (Float, Float) -> (Float, Float)
complexAdd (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

(|+) z1 z2 = complexAdd z1 z2

complexMagnitude :: (Float, Float) -> Float
complexMagnitude (x, y) = sqrt (x * x + y * y)


max_iter = 100
mandelbrotIterator :: (Float, Float) -> Maybe Int
mandelbrotIterator c = iter (0, 0) c 0
  where
    iter z c n
      | n == max_iter = Nothing
      | complexMagnitude z >= 2.0 = Just n
      | otherwise = iter ((z |* z) |+ c) c (n + 1) 
    

getMandelbrotPainter :: Int -> Int -> Point -> Maybe Color
getMandelbrotPainter width height = painter
  where
    coord_x x = (fromIntegral x) / (fromIntegral width) * (1.5 - (-2.0)) - 2.0
    coord_y y = (fromIntegral y) / (fromIntegral height) * (2.0 - (-2.0)) - 2.0
    coord (x, y) = (coord_x x, coord_y y)
    mapToColor Nothing = (0, 0, 0)
    mapToColor (Just n) = (fromIntegral n, fromIntegral n, fromIntegral $ n * 2)
    painter pt = Just $ mapToColor (mandelbrotIterator (coord pt))

    
mandelCanvas = Canvas { painters = [getMandelbrotPainter width height],
                        getWidth = width,
                        getHeight = height,
                        getBackgroundColor = backgroundColor }




main = do
  -- putStrLn $ show $ map testFunction [0..10]
  -- putStrLn $ show $ mapMaybe testFunction [0..10]
  -- renderASCII asciiCanvas
  --success <- renderPPM "test.ppm" testCanvas
  success <- renderPPM "mandel.ppm" mandelCanvas
  putStrLn $ "Saving was successful: " ++ show success
  -- putStrLn $ show $ groupN 3 $ [0..10]
  -- let iter = mandelbrotIterator (-1.0, 1.0)
 


